export EvolveNaiveState, EvolveDependenciesState, EvolveAccumDiffState
export evolve!
using Printf


abstract type EvolveState end

gettotalrate(es::EvolveState) = es.totalrate


"""
    evolve!(rng, rs::AbstractReactionSystem,
            es = EvolveNaiveState(rs),
            tmax::Float64 = Inf64)

Evolves the stochastic reaction system `rs` using the random number generator
`rng` until (and including the execution of) the next event/reaction, but to a
maximum of `tmax`. The return value indicates if another call of `evolve!` with
the same arguments would have any effect: Returns `false` if the `tmax` barrier
was hit, in which case `gettime(rs)==tmax` *exactly* after the execution of
`evolve!`. Returns `true` otherwise. The behavior for `tmax < gettime(rs)` is
undefined.

The algorithm used to carry out Gillespie-type simulations including additional
events (e.g., from delayed reactions) is determined by the type of `es`, which
contains the internal state of the algorithm. It should be created at the
beginning of the simulation and then passed subsequently to each call of
`evolve!`. Currently, [`EvolveNaiveState`](@ref),
[`EvolveDependenciesState`](@ref) and [`EvolveAccumDiffState`](@ref) are
available.
""" evolve!


"""
    EvolveNaiveState(rs::AbstractReactionSystem)

State used for [`evolve!`](@ref) calls for the naive Gillespie algorithm. This
algorithm is efficient when reactions are strongly coupled, i.e. the majority of
reaction rates change when any reaction is executed. It recalculates all the
rates in every step. Does not require the presence of a reaction network
[`topology`](@ref) for the reaction system.
"""
mutable struct EvolveNaiveState <: EvolveState
    rates::Vector{Float64}
    csrates::Vector{Float64}
    totalrate::Float64
    function EvolveNaiveState(rs::AbstractReactionSystem)
        state = new(Vector{Float64}(undef, length(getreactions(rs))),
                    Vector{Float64}(undef, length(getreactions(rs))),
                    0)
            resetstate!(rs,state)
            return state
    end
end

function resetstate!(rs::AbstractReactionSystem, state::EvolveNaiveState)
    rates = state.rates
    csrates = state.csrates
    reactions = getreactions(rs)
    for (i,r) ∈ enumerate(reactions)
        rates[i] = getrate(rs,r)
    end
    cumsum!(csrates, rates)
    state.totalrate = csrates[end]
end

function evolve!(rng, rs::AbstractReactionSystem,
                       state::EvolveNaiveState=EvolveNaiveState(rs),
                       tmax::Float64=Inf64)
    csrates = state.csrates

    
    if state.totalrate <= 0.
        Δt = Inf
    else
        Δt = -log(rand(rng))/state.totalrate
    end
    newtime = gettime(rs) + Δt
    net = getnexteventtime(rs)
    event = false
    if newtime >= net
        newtime = net
        event = true
    end
    if newtime >= tmax
        settime!(rs, tmax)
        resetstate!(rs, state)
        return false
    end
    if event
        react = popnextreaction!(rs)
        choosefrom = getevents(rs)
    else
        r = state.totalrate*rand(rng)
        react = -1
        for i in 1:length(csrates)
            if r <= csrates[i]
                react = i
                break
            end
        end
        choosefrom = getreactions(rs)
    end
    #@printf "Reaction: %d\n" react
    settime!(rs, newtime)
    executereaction!(rs, choosefrom[react])
    
    resetstate!(rs, state)
    
    return true
end



"""
    EvolveDependenciesState(rs::AbstractReactionSystem, resetsteps::Int64)

State used for [`evolve!`](@ref) calls for an improved version of the Gillespie
algorithm. This algorithm is efficient when reactions are only weakly coupled,
i.e. only a fraction of the reaction rates change when a typical reaction is
executed. Optimizations of this algorithm include the recalculation of rates
only for the affected reactions, subsequent adjustment of the total rate, and
on-the-fly calculation of cumulative rates.

`resetsteps` specifies the number of simulation steps after which *all* the
rates and quantities dependent on them are recalculated, to avoid the indefinite
accumulation of floating point errors. In addition, an exact caclulation is
triggered when the total rate drops to less than one thousandth of the previous
value in one step (to detect a possible exact zero and to avoid cancellation
errors).

Requires the presence of a reaction network [`topology`](@ref) for the reaction
system.
"""
mutable struct EvolveDependenciesState <: EvolveState
    rates::Vector{Float64}
    csrates::Vector{Float64}
    csdone::Int64
    totalrate::Float64
    accuracy::Float64
    steps::Int64
    resetsteps::Int64
    function EvolveDependenciesState(rs::AbstractReactionSystem, resetsteps::Int64)
        reactions = getreactions(rs)
        state = new(Vector{Float64}(undef,length(reactions)),
                    Vector{Float64}(undef,length(reactions)),
                    0, NaN,0.,0,resetsteps)
        resetstate!(rs, state)
        # Have to reset, because first will have yielded a huge deviation
        state.accuracy = 0
        return state
    end
end

function resetstate!(rs::AbstractReactionSystem, state::EvolveDependenciesState)
    oldtotalrate = state.totalrate
    rates = state.rates
    csrates = state.csrates
    reactions = getreactions(rs)
    for (i,r) ∈ enumerate(reactions)
        rates[i] = getrate(rs,r)
    end
    cumsum!(csrates, rates)
    state.csdone = length(rates)
    state.totalrate = csrates[end]
    state.steps = 0
    state.accuracy = max(state.accuracy, abs(state.totalrate-oldtotalrate))
end

function evolve!(rng, rs::AbstractReactionSystem,
                       state::EvolveDependenciesState,
                       tmax::Float64=Inf64)
    rates = state.rates
    csrates = state.csrates

    
    if state.totalrate <= 0.
        Δt = Inf
    else
        Δt = -log(rand(rng))/state.totalrate
    end
    newtime = gettime(rs) + Δt
    net = getnexteventtime(rs)
    event = false
    if newtime >= net
        newtime = net
        event = true
    end
    if newtime >= tmax
        settime!(rs, tmax)
        # So we start subsequent evolve calls with a clean state
        resetstate!(rs, state)
        return false
    end
    if event
        react = popnextreaction!(rs)
        choosefrom = getevents(rs)
        affectedreactions = getreactionsaffectedbyevent(rs, react)
    else
        r = state.totalrate*rand(rng)
        react = -1
        for i in 1:length(csrates)
            if i > state.csdone
                csrates[i] = csrates[i-1] + rates[i]
                state.csdone = i
            end
            if r <= csrates[i]
                react = i
                break
            end
        end
        if react<0
            @warn "react is <=0, totalrate $(state.totalrate), realrate $(sum(rates))"
            settime!(rs, newtime)
            return true
        end
        choosefrom = getreactions(rs)
        affectedreactions = getreactionsaffectedbyreaction(rs, react)
    end
    #@printf "Reaction: %d\n" react
    settime!(rs, newtime)
    executereaction!(rs, choosefrom[react])
    
    if isempty(affectedreactions)
        return true
    end
    reactions = getreactions(rs)
    state.csdone = minimum(affectedreactions)-1
    oldtotalrate = state.totalrate
    for i ∈ affectedreactions
        newrate = getrate(rs,reactions[i]);
        state.totalrate += newrate-rates[i]
        state.steps += 1
        rates[i] = newrate
    end
    # Second condition: If rate drops by a significant factor in a single step,
    # it probably means the new rate is 0, so calculate a clean state
    # to detect this exactly
    if state.steps >= state.resetsteps ||
        abs(state.totalrate/oldtotalrate)<1e-3
        resetstate!(rs,state)
    end
    
    if state.csdone==0
        csrates[1] = rates[1]
        state.csdone = 1
    end

    return true
end




"""
    EvolveAccumDiffState(rs::AbstractReactionSystem, changefac::Float64)

State used for [`evolve!`](@ref) calls for an even more optimized version of the
Gillespie algorithm. This algorithm is efficient when reactions are only weakly
coupled, i.e. only a fraction of the reaction rates change when a typical
reaction is executed, and the reactions that change are roughly the same over
long periods of time. Optimizations of this algorithm include the recalculation
of rates only for the affected reactions, subsequent adjustment of the total
rate, keeping track of changes relative to the last recorded exact set of
reaction rates, and on-the-fly calculation of cumulative rates.

`0 < changefac < 1` specifies the fraction of reactions rates whose changes are
"cached" by the algorithm before *all* the rates and quantities dependent on
them are recalculated. In addition, an exact caclulation is triggered when the
total rate drops to less than one thousandth of the previous value in one step
(to detect a possible exact zero and to avoid cancellation errors).

Requires the presence of a reaction network [`topology`](@ref) for the reaction
system.
"""
mutable struct EvolveAccumDiffState <: EvolveState
    origrates::Vector{Float64}
    
    addreactions::Vector{Int64}
    rates::Vector{Float64}
    csrates::Vector{Float64}
    totalrate::Float64
    realrate::Float64
    csdone::Int64
    numadded::Int64
    
    function EvolveAccumDiffState(rs::AbstractReactionSystem, changefac::Float64)
        
        reactions = getreactions(rs)
        numadditional = Int64(round(changefac*length(reactions)))
        
        @info "Allocating $numadditional additional reaction slots"
        
        instance = new(Vector{Float64}(undef,length(reactions)),
                    zeros(Int64, length(reactions)+numadditional),
                    Vector{Float64}(undef, length(reactions)+numadditional),
                    Vector{Float64}(undef, length(reactions)+numadditional),
                    0., length(reactions), 0)
        
        resetstate!(rs,instance)
        return instance
    end
end

function resetstate!(rs::AbstractReactionSystem, state::EvolveAccumDiffState)
    reactions = getreactions(rs)
    for (i,r) ∈ enumerate(reactions)
        state.origrates[i] = getrate(rs,r)
        state.rates[i] = state.origrates[i]
        if i==1
            state.csrates[i] = state.origrates[i]
        else
            state.csrates[i] = state.csrates[i-1] + state.origrates[i]
        end
    end
    for i ∈ length(reactions)+1:length(state.rates)
        state.rates[i] = 0.
        state.csrates[i] = 0.
    end
    state.totalrate = state.csrates[length(reactions)]
    state.realrate = state.totalrate
    
    @. state.addreactions = 0
    state.csdone = length(reactions)
    state.numadded = 0
    
end


function evolve!(rng, rs::AbstractReactionSystem,
                 state::EvolveAccumDiffState,
                 tmax::Float64=Inf64)
    origrates = state.origrates
    addreactions = state.addreactions
    
    rates = state.rates
    csrates = state.csrates
    if state.realrate < 0.5*state.totalrate
        # @info "Resetting state because realrate too small"
        resetstate!(rs, state)
    end
    
    if state.totalrate <= 0.
        Δt = Inf
    else
        Δt = -log(rand(rng))/state.totalrate
    end
    newtime = gettime(rs) + Δt
    net = getnexteventtime(rs)
    event = false
    if newtime >= net
        newtime = net
        event = true
    end
    if newtime >= tmax
        settime!(rs, tmax)
        # So we start subsequent evolve calls with a clean state
        resetstate!(rs, state)
        return false
    end
    if event
        react = popnextreaction!(rs)
        choosefrom = getevents(rs)
        affectedreactions = getreactionsaffectedbyevent(rs, react)
    else
        r = state.totalrate*rand(rng)
        react = -1
        for i in 1:length(origrates)+state.numadded
            if i > state.csdone
                csrates[i] = csrates[i-1] + rates[i]
                state.csdone = i
            end
            if r <= csrates[i]
                react = i
                break
            end
        end
        if react<0
            @warn "react is <=0, totalrate $(state.totalrate), tracked realrate $(state.realrate), current numerical total $(sum(origrates)+sum(rates[length(origrates)+1:end])), true real rate $(sum(rates[1:length(origrates)]))"
            settime!(rs, newtime)
            return true
        end
        # @printf "Literal reaction: %d\n" react
        if react <= length(origrates)
            if (rates[react]>=origrates[react]) ||
                (rand(rng) < rates[react]/origrates[react])
                #continue with this react
            else
                settime!(rs, newtime)
                return true
            end
        else
            react = addreactions[react]
            #continue with this react
        end
        # @printf "True reaction: %d\n" react
        @assert react<=length(origrates) "react is not <= length(origrates)"
        choosefrom = getreactions(rs)
        affectedreactions = getreactionsaffectedbyreaction(rs, react)
    end
    settime!(rs, newtime)
    executereaction!(rs, choosefrom[react])
    
    if isempty(affectedreactions)
        return true
    end
    reactions = getreactions(rs)
    if length(reactions)+state.numadded+length(affectedreactions)>length(addreactions)
        # @info "Resetting state because length too small"
        resetstate!(rs, state)
        return true
    end
    # @info "real/total = $(state.realrate/state.totalrate)"
    oldtotalrate = state.totalrate
    for i ∈ affectedreactions
        newrate = getrate(rs,reactions[i])
        effprev = max(origrates[i], rates[i])
        effnew = max(origrates[i], newrate)
        state.totalrate += effnew-effprev
        state.realrate += newrate-rates[i]
        if effnew > origrates[i]
            if addreactions[i]==0
                state.numadded += 1
                addedidx = length(reactions)+state.numadded
                addreactions[i] = addedidx
                addreactions[addedidx] = i
            end
            rates[addreactions[i]] = newrate-origrates[i]
            state.csdone = min(state.csdone, addreactions[i]-1)
        else
            if addreactions[i] > 0 && rates[addreactions[i]]>0.
                state.csdone = min(state.csdone, addreactions[i]-1)
                rates[addreactions[i]] = 0.
            end
        end
        rates[i] = newrate
    end
    # If rate drops by a significant factor in a single step,
    # it probably means the new rate is 0, so calculate a clean state
    # to detect this exactly
    if abs(state.totalrate/oldtotalrate)<1e-3
        resetstate!(rs,state)
    end
    # @info "real/total = $(state.realrate/state.totalrate)"
    return true
end


