export distributerandomly, distributeevenly
export evolveandrecordstates!

using Printf
using Random


"""
    distributerandomly(rng, n, g)

Distributes `n` elements randomly across `g` groups using the random number
generator `rng`. Returns a vector of length `g` containing the resulting number
of elements in each group (with `sum(vec) == n`).
"""
function distributerandomly(rng, nelements,ngroups)
     choice = rand(rng,1:ngroups,nelements)
     ret = fill(0,ngroups)
     for g ∈ choice
         ret[g] = ret[g] + 1
     end
     return ret
end

"""
    distributeevenly(n, g)

Distributes `n` elements evenly across `g` groups. Returns a vector of length
`g` containing the resulting number of elements in each group (with `sum(vec) ==
n`). If `n` is not divisible by `g`, places the remaining elements into the
first `n % g` groups.
"""
function distributeevenly(nelements,ngroups)
     min = nelements ÷ ngroups
     more = nelements % ngroups
     ret = fill(min,ngroups) + 
            [fill(1,more); fill(0,ngroups-more)]
     return ret
end

"""
    evolveandrecordstates!(rng::AbstractRNG, es::EvolveState,
                           rs::AbstractReactionSystem,
                           timepoints; Δtsample=nothing)

Convenience function to evolve the reaction system `rs` and record its state (as
obtained through [`getfullstate`](@ref)) exactly at the time points specified
via the vector `timepoints`. This is done by repeatedly calling
[`evolve!`](@ref) on `rs` using the random number generator `rng`. Stops after
`timepoints[end]` unless `Δtsample` is defined. To record system states at
regular intervals indefinitely, specify an empty `Vector{Float64}()` for
`timepoints` and instead specify `Δtsample`.

Returns a vector with all the recorded states and modifies `timepoints` if
`Δtsample` is specified.

Other arguments:

*   `es`:
    A subtype of `EvolveState` specifying which algorithm to use for the
    simulation of the system. See [`evolve!`](@ref) for details.

Optional arguments:

*   `Δtsample`:
    If specified, `timepoints` is extended with time points separated by
    `Δtsample` as needed until the end of the simulation is reached (i.e. all
    reaction rates are zero and there are no remaining events to execute).

"""
function evolveandrecordstates!(rng::AbstractRNG, es::EvolveState, rs::AbstractReactionSystem, timepoints; Δtsample=nothing)
    t = gettime(rs)
    if length(timepoints)==0
        push!(timepoints,t)
    end
    recidx = 1
    states = Vector{Any}()
    while recidx <= length(timepoints)
        if gettime(rs)==timepoints[recidx]
            st = getfullstate(rs)
            push!(states,st)
            #@printf "%d: %d (t=%f, totalrate=%f)\n" recidx maximum(st["I"]) gettime(rs) sum(getrates(rs))
            recidx += 1
            if recidx > length(timepoints)
                if !isnothing(Δtsample)
                    push!(timepoints,timepoints[end]+Δtsample)
                else
                    break
                end
            end
        end
        while(evolve!(rng, rs, es, timepoints[recidx])) end
        # println(timepoints[recidx], " ", gettotalrate(es), " ", rs.Itotal, " ", rs.Itotallockdown)
        
        # Have to check this afterwards so does not break if
        # the initial t is not in tarray
        if gettotalrate(es)==0. && isinf(getnexteventtime(rs))
            resize!(states,length(timepoints))
            states[recidx:end] .= fill(getfullstate(rs),length(states)-recidx+1)
            break
        end
    end
    return states
end

