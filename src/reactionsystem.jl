export AbstractReactionSystem
export AbstractReaction
export Reactant
export @rate, @reaction, @event, @conditionalschedule
export topology
export getfullstate, getfullstate!


abstract type AbstractTracer end
const Reactant = Union{Int64, AbstractTracer}                      

"""
Supertype for all reaction systems. If the reaction system shall make use of the
automatic dependency detection via [`topology`](@ref), all fields in the
reaction system and its inner structs that represent species counts (and would
ordinarily be Int64), should have the type `R`. During dependency detection, `R`
will be a tracer type that records reaction dependencies, whereas the actual
simulated reaction system will use `R == Int64`.

It is recommended to organize the dependency detection in the following way:

1.  The constructor taking the type parameter `R` should initialize the
    reaction system as required, except the fields which hold the dependencies
    as returned by the [`topology`](@ref) function. The setup of
    reactions/events and their interdependencies should not depend on the value
    of `R`, as this will break automatic dependency detection.
    
2.  An outer constructor (taking no type parameter) should produce an
    instance with `R = Int64` by forwarding all arguments to the constructor
    above. It can then fill the remaining topology fields by calling
    [`topology`](@ref), which will use a different value for `R` to
    automatically detect the dependencies.
"""
abstract type AbstractReactionSystem{R<:Reactant} end

"""
Supertype for all reactions. The reaction structs should hold references to all
required reactants that are not available directly through the reaction system,
so they can be accessed for rate calculation and reaction execution.
    
If the reaction system shall make use of the automatic dependency detection via
[`topology`](@ref), the reaction is required to have a type parameter (e.g.,
`R`) that it can forward to the structs (that contain the reactants) which it
holds references to. During dependency detection, `R` will be a tracer type that
records reaction dependencies, whereas the actual simulation will use `R ==
Int64`.
"""
abstract type AbstractReaction end

function getrate(rs::AbstractReactionSystem, r::AbstractReaction) end
function executereaction!(rs::AbstractReactionSystem, r::AbstractReaction) end
function tryscheduleevent!(rs::AbstractReactionSystem, r::AbstractReaction, i::Int64) end

"""
Obtain a full representation of the current state of the reaction system `rs`,
excluding time.
"""
function getfullstate(rs::AbstractReactionSystem) end
"""
Obtain a full representation of the current state of the reaction system `rs`,
excluding time, and write the result into the preallocated `state`.
"""
function getfullstate!(state, rs::AbstractReactionSystem) end


"""
Obtain the current time of the reaction system `rs`. Should be overloaded by
specific model if time is not saved in field `t` of the reaction system.
"""
gettime(rs::AbstractReactionSystem) = rs.t

"""
Set the current time of the reaction system `rs` to `t`.
"""
function settime!(rs::AbstractReactionSystem, t::Float64)
    rs.t = t
end

"""
    getreactions(rs::AbstractReactionSystem)
    
Get a list of all reactions (e.g., a `Vector{AbstractReaction}`) that can
potentially happen in the reaction system. This list, and in particular the
ordering, must not change throughout a simulation in order for the current
algorithms to work correctly. The general method assumes that the reactions are
saved in a the field `rs.reactions` of the reaction system. Otherwise, the
function should be overloaded for the specific reaction system type.
"""
getreactions(rs::AbstractReactionSystem) = rs.reactions

"""
    getreactionsaffectedbyreaction(rs::AbstractReactionSystem, i::Int64)
    
Get a list of all reactions whose rates could be changed by an execution of
reaction `i` (as an index into the list returned by [`getreactions`](@ref)). The
general method assumes that this information can be obtained by accessing
`rs.network[i]`, as produced by the automatic dependency detection via the
[`topology`](@ref). Otherwise, the function should be overloaded for the
specific reaction system type.
"""
getreactionsaffectedbyreaction(rs::AbstractReactionSystem, i::Int64) = rs.network[i]

"""
    getnexteventtime(rs::AbstractReactionSystem)
    
Returns the time of the next scheduled event. Should return `Inf` if there is no
event scheduled. The default method always returns `Inf` and should be
overloaded for the specific reaction system type if it makes use of events. The
use of a `PriorityQueue{Int64,Float64}` for managing events internally is
recommended.
"""
getnexteventtime(rs::AbstractReactionSystem) = Inf
"""
    popnextreaction!(rs::AbstractReactionSystem)

Returns the event (as an index into the event list as returned by
[`getevents`](@ref)) will happen next and removes it from the event queue. If a
`PriorityQueue{Int64,Float64}` is used for keeping track of scheduled events
internally, this is simply a call to `dequeue!`.
"""
popnextreaction!(rs::AbstractReactionSystem) = -1

"""
    getevents(rs::AbstractReactionSystem)
    
Get a list of all events (e.g., a `Vector{AbstractReaction}`) that can
potentially happen in the reaction system. This list, and in particular the
ordering, must not change throughout a simulation in order for the current
algorithms to work correctly. The general method assumes that the events are
saved in a the field `rs.events` of the reaction system. Otherwise, the function
should be overloaded for the specific reaction system type.
"""
getevents(rs::AbstractReactionSystem) = rs.events

"""
    getreactionsaffectedbyevent(rs::AbstractReactionSystem, i::Int64)
    
Get a list of all reactions whose rates could be changed by an execution of
event `i` (as an index into the list returned by [`getevents`](@ref)). The
general method assumes that this information can be obtained by accessing
`rs.eventstoreactions[i]`, as produced by the automatic dependency detection via
the [`topology`](@ref). Otherwise, the function should be overloaded for the
specific reaction system type.
"""
getreactionsaffectedbyevent(rs::AbstractReactionSystem, i::Int64) = rs.eventstoreactions[i]

mutable struct Tracer <: AbstractTracer
    reactionindices::Vector{Int64}
end

Base.convert(::Type{Tracer}, ::Int64) = Tracer(Int64[])


macro traceroperator(op)
    return quote
    Base.$op(::Tracer, ::T) where T = 0
    Base.$op(::T, ::Tracer) where T = 0
    Base.$op(::Tracer, ::Tracer) = 0
    end
end

@traceroperator(+)
@traceroperator(-)
@traceroperator(*)
@traceroperator(/)


function registerinputs(sp::AbstractReactionSystem, r::AbstractReaction, i::Int64) end
function products(sp::AbstractReactionSystem, r::AbstractReaction) end

register!(tr::Tracer, i::Integer) = push!(tr.reactionindices, i)


function getrates(rs::AbstractReactionSystem)
    reactions = getreactions(rs)
    ret = Vector{Float64}(undef,length(reactions))
    for (i,r) ∈ enumerate(reactions)
        ret[i] = getrate(rs,r)
    end
    return ret
end


"""
    @rate system reaction functionbody reactants...

Macro to define the rate and dependencies for a reaction.

Parameters:

*   `system`: type of reaction system struct
*   `reaction`: type of reaction struct for which the rate is being defined
*   `functionbody`:
    begin-end block containing the code for calculating the rate. Inside
    this function body, two arguments are accessible:

    *   `s`: the reaction system
    *   `r`: the reaction for which the rate is calculated.

*   `reactants`:
    Arbitrary number of arguments listing all the reactants inside `s` and `r`
    on which this rate depends, i.e. the rate will only change when any of the
    listed reactants change in number/state. This information is used by the
    [`topology`](@ref) function to calculate the dependency network between
    reactions/events, i.e. the listed fields must be of type `R`, where `R` is
    the type parameter of the reaction system (see
    [`AbstractReactionSystem`](@ref), [`AbstractReaction`](@ref)).

"""
macro rate(system, reaction, functionbody, reactants...)
    #system = esc(system)
    #reaction = esc(reaction)
    
    register = [:(GeneralizedStochasticSimulations.register!($r, i)) for r ∈ reactants]
    return esc(quote
        function GeneralizedStochasticSimulations.getrate(s::$system{Int64}, r::$reaction{Int64})
            $functionbody
        end

        function GeneralizedStochasticSimulations.registerinputs(s::$system{GeneralizedStochasticSimulations.Tracer}, r::$reaction, i::Int64)
            $(register...)
        end
    end)
end

"""
    @conditionalschedule system reaction functionbody reactants...

Macro for scheduled event based on condition (dependent on reactants just like rate
for reactions). This macro is intended for future usage and currently
non-functional because evolve! algorithms do not call tryscheduleevent.
"""
macro conditionalschedule(system, reaction, functionbody, reactants...)
    #system = esc(system)
    #reaction = esc(reaction)
    
    register = [:(GeneralizedStochasticSimulations.register!($r, i)) for r ∈ reactants]
    return esc(quote
        function GeneralizedStochasticSimulations.tryscheduleevent!(s::$system{Int64}, r::$reaction{Int64}, i::Int64)
            $functionbody
        end

        function GeneralizedStochasticSimulations.registerinputs(s::$system{GeneralizedStochasticSimulations.Tracer}, r::$reaction, i::Int64)
            $(register...)
        end
    end)
end

"""
    @reaction system reaction functionbody reactants...

Macro to define the system changes caused by a reaction.

Parameters:

*   `system`: type of reaction system struct
*   `reaction`: type of reaction struct whose execution is being defined
*   `functionbody`:
    begin-end block containing the code for executing the reaction. Inside
    this function body, two arguments are accessible:

    *   `s`: the reaction system
    *   `r`: the reaction whose execution is being defined.

*   `reactants`:
    Arbitrary number of arguments listing all the reactants inside `s` and `r`
    affected by this reaction, i.e. whose numbers/states could possibly change
    upon execution. This information is used by the [`topology`](@ref) function
    to calculate the dependency network between reactions/events, i.e. the
    listed fields must be of type `R`, where `R` is the type parameter of the
    reaction system (see [`AbstractReactionSystem`](@ref),
    [`AbstractReaction`](@ref)).

"""
macro reaction(system, reaction, expr, products...)
    #system = esc(system)
    #reaction = esc(reaction)
    
    return esc(quote
        function GeneralizedStochasticSimulations.executereaction!(s::$system{Int64}, r::$reaction{Int64})
            $expr
        end

        function GeneralizedStochasticSimulations.products(s::$system{GeneralizedStochasticSimulations.Tracer}, r::$reaction)
            return [$(products...)]
        end
    end)
end

"""
    @event system reaction functionbody reactants...

Macro to define the system changes caused by an event (which is also an
[`AbstractReaction`](@ref)).

Parameters:

*   `system`: type of reaction system struct
*   `reaction`: type of reaction struct whose execution is being defined
*   `functionbody`:
    begin-end block containing the code for executing the event. Inside
    this function body, two arguments are accessible:

    *   `s`: the reaction system
    *   `r`: the event whose execution is being defined.

*   `reactants`:
    Arbitrary number of arguments listing all the reactants inside `s` and `r`
    affected by this reaction, i.e. whose numbers/states could possibly change
    upon execution. This information is used by the [`topology`](@ref) function
    to calculate the dependency network between reactions/events, i.e. the
    listed fields must be of type `R`, where `R` is the type parameter of the
    reaction system (see [`AbstractReactionSystem`](@ref),
    [`AbstractReaction`](@ref)).
"""
var"@event" = var"@reaction"


"""
    topology(RSType::Type{<:AbstractReactionSystem}, args...;
    sourcereactionsfunc=getreactions, targetreactionsfunc=getreactions, kwargs...)

Calculates the dependency network between the list of reactions returned by
`sourcereactionsfunc` and `targetreactionsfunc` for the reaction system of type
`RSType`. The function returns a `Vector{Vector{Int64}}`, whose `i`-th element
is a Vector [i1, i2, i3, ...] of the indices of all the reactions affected by
reaction `i`, where `i` is an index into the list returned by
`sourcereactionsfunc` and `i1`, `i2`, `i3`,... are indices into the list
returned by `targetreactionsfunc`. Both funcs default to [`getreactions`](@ref).

See [`AbstractReactionSystem`](@ref), [`getreactionsaffectedbyreaction`](@ref)
and [`getreactionsaffectedbyevent`](@ref) on how to use this output. In
particular, the reaction system struct type must be able to take a type
parameter which can be set to a `Tracer` type.

`args` and `kwargs` are forwarded as constructor arguments to build the reaction
system.
"""
function topology(RSType::Type{<:AbstractReactionSystem}, args...; sourcereactionsfunc=getreactions, targetreactionsfunc=getreactions, kwargs...)
    
    s = RSType{Tracer}(args...; kwargs...)
    targetreactions = targetreactionsfunc(s)
    
    for (i, r) ∈ enumerate(targetreactions)
        registerinputs(s, r, i)
    end
    
    sourcereactions = sourcereactionsfunc(s)
    
    network = Vector{Vector{Int64}}(undef, length(sourcereactions))

    for (i, r) ∈ enumerate(sourcereactions)
        network[i] = unique(vcat(map(x->x.reactionindices, products(s, r))...))
    end
    
    return network
end
                          
        
        
