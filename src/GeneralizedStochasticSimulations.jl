module GeneralizedStochasticSimulations

using Pkg

const VERSION = "0.1.0"


"""
       TREE_HASH
Contains the tree hash of the loaded version of the module at precompile time.
Look for git commits with this tree hash by parsing `git log --pretty=raw` or
by using the helper script `tree2commit`.
"""
const TREE_HASH = bytes2hex(Pkg.GitTools.tree_hash(joinpath(splitpath(@__FILE__)[1:end-2]...)))

include("reactionsystem.jl")
include("evolve.jl")
include("utils.jl")

end # module
