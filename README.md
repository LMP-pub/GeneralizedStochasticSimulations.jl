# GeneralizedStochasticSimulations.jl

GeneralizedStochasticSimulations is a framework to carry out stochastic simulations of reaction systems with arbitrary rate dependencies and the possibility to incorporate additional discrete events and delays. The species, reactions, rate functions and events are all defined using arbitrary structs/code and are thus not restricted to mass-action type chemical reactions (hence "Generalized").

The main simulation algorithm is the Gillespie algorithm, with two additional versions providing a particular speedup for reaction systems with many reactions that are only weakly connected. Weak connections mean that the execution of one reaction only changes the rates of a fraction of reactions in the whole system. The required information about the network of connections is extracted semi-automatically from the reaction system.

This is a snapshot of the simulation framework that enables simulations for the manuscript pushlished [here](https://doi.org/10.1016/j.eclinm.2020.100718). It is installed automatically as a dependency of the [simulation code](https://gitlab.gwdg.de/LMP-pub/localcontainment) published therein. Manual installation of this framework should therefore not be necessary.

Active development of this software takes place in a different repository which is not public at the time of this writing. Please contact Philip Bittihn at philip.bittihn@ds.mpg.de if you are interested.
